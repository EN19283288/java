package io.techsaavy.array;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        new ArrayCreation().createArray();

        final int[] arrayWithRange1 = new ArrayCreation().createArrayWithRange(3);
        System.out.println("Arrays.toString(arrayWithRange1) = " + Arrays.toString(arrayWithRange1));
        System.out.println();

        final String[] arrayWithRange2 = new ArrayCreation().createArrayWithRange("Test");
        System.out.println("Arrays.toString(arrayWithRange2) = " + Arrays.toString(arrayWithRange2));
        System.out.println();

        final Integer[] arrayWithStream1 = new ArrayCreation().createArrayWithStream(10);
        System.out.println("Arrays.toString(arrayWithStream1) = " + Arrays.toString(arrayWithStream1));
        System.out.println();
    }
}
