package io.techsaavy.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by Michael Lossagk
 * <p>
 * Array creation examples
 */
public class ArrayCreation {

    /**
     * ---------------------------------------------------------------------------------------------------------------------------------
     * createArray
     * ---------------------------------------------------------------------------------------------------------------------------------
     */
    public void createArray() {

        // primitive types
        int[] myIntArray1 = new int[3];
//        int myIntArray1[] = new int[3];
        int[] myIntArray2 = new int[]{1, 2, 3};
        int[] myIntArray3 = {1, 2, 3};

        // classes
        String[] myStringArray1 = new String[3];
        String[] myStringArray2 = new String[]{"a", "b", "c"};
        String[] myStringArray3 = {"a", "b", "c"};
        String[] myStringArray4 = {new String("a"), new String("b"), new String("c")};

        myIntArray1[0] = 0;
        myStringArray1[0] = "a";

        System.out.println("myIntArray1 = " + Arrays.toString(myIntArray1));
        System.out.println("myIntArray2 = " + Arrays.toString(myIntArray2));
        System.out.println("myIntArray3 = " + Arrays.toString(myIntArray3));

        System.out.println("myStringArray1 = " + Arrays.toString(myStringArray1));
        System.out.println("myStringArray2 = " + Arrays.toString(myStringArray2));
        System.out.println("myStringArray3 = " + Arrays.toString(myStringArray3));
        System.out.println("myStringArray4 = " + Arrays.toString(myStringArray4));

        System.out.println();
    }


    /**
     * ---------------------------------------------------------------------------------------------------------------------------------
     * create an array
     *
     * @return range
     * ---------------------------------------------------------------------------------------------------------------------------------
     */
    public int[] createArrayWithRange(int rangeNumber) {
        int[] range = new int[rangeNumber];

        for (int i = 1; i <= range.length; ++i) {
            range[i - 1] = i;
        }
        return range;
    }

    /**
     * ---------------------------------------------------------------------------------------------------------------------------------
     * create an array
     *
     * @return range
     * ---------------------------------------------------------------------------------------------------------------------------------
     */
    public String[] createArrayWithRange(String str) {
        String[] range = new String[str.length()];

        for (int i = 1; i <= range.length; i++) {
            range[i - 1] = String.valueOf(str.charAt(i - 1));
        }
        return range;
    }

    /**
     * ---------------------------------------------------------------------------------------------------------------------------------
     * create an array
     *
     * @return range
     * ---------------------------------------------------------------------------------------------------------------------------------
     */
    public Integer[] createArrayWithStream(int number) {
        Integer[] range = IntStream.range(0, number)
                .mapToObj(i -> new Integer((int) (Math.random() * number + 1)))
                .toArray(Integer[]::new);
        return range;
    }
}
