package io.techsaavy.fizzbuzz;

public class Main {

    public static void main(String[] args) {

        System.out.println("doFizzBuzzV1");
        new FizzBuzz().doFizzBuzzV1();

        System.out.println("\ndoFizzBuzzV2");
        new FizzBuzz().doFizzBuzzV2();

        System.out.println("\ndoFizzBuzzWrongV1");
        new FizzBuzz().doFizzBuzzWrongV1();

        System.out.println("\ndoFizzV1");
        new FizzBuzz().doFizzV1();

        System.out.println("\ndoBuzzV1");
        new FizzBuzz().doBuzzV1();

        System.out.println("\ndoFizzBuzzStandaloneV1");
        new FizzBuzz().doFizzBuzzStandaloneV1();

        System.out.println();
    }
}
